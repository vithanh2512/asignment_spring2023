<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!doctype html>
<html lang="en">
<head>
<title>Title</title>
<!-- Required meta tags -->
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<base href="/asignment2/">
<!-- Bootstrap CSS -->
<link
	href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.1/dist/css/bootstrap.min.css"
	rel="stylesheet"
	integrity="sha384-iYQeCzEYFbKjA/T2uDLTpkwGzCiq6soy8tYaI1GyVh/UjpbCx/TYkiZhlZB6+fzT"
	crossorigin="anonymous">

<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.2.1/css/all.min.css">

<link rel="stylesheet"
	href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.10.3/font/bootstrap-icons.css">
<link rel="stylesheet" type="text/css"
	href="https://cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css" />
<link rel="stylesheet" href="asmwebb/Websitebanhang/css/index.css">

<script
	src="https://cdnjs.cloudflare.com/ajax/libs/angular.js/1.8.3/angular.min.js"
	integrity="sha512-KZmyTq3PLx9EZl0RHShHQuXtrvdJ+m35tuOiwlcZfs/rE7NZv29ygNA8SFCkMXTnYZQK2OX0Gm2qKGfvWEtRXA=="
	crossorigin="anonymous" referrerpolicy="no-referrer"></script>

</head>
<body style="background-image: url('img/hinh-nen-4k.jpg');">
	<%@include file="/asmwebb/Websitebanhang/JSP/header.jsp"%>
	<div class="container col-8 pt-2" >
		<div class="border rounded  pl-4 pr-4 p-3">
			<div class="row p-2 pl-3 pt-3  text-dark rounded" style="background-color: #E5E5E5;">
				<h2>LIST USERS</h2>
			</div>
			<div class="row p-2">
				<div class="col">
					<c:if test="${not empty message}">
						<div class="alert alert-success bg-info">${message}</div>
					</c:if>
					<c:if test="${not empty error}">
						<div class="alert alert-danger">${error}</div>
					</c:if>
				</div>
			</div>
			<div class="row" >
				<div class="col">
					<form action="user" method="post" >
					
						<div class="form-group">
							<label>User ID:</label> <input type="text" class="form-control" name="id" value="${id}" />
						</div>
						<div class="form-group">
							<label>Password:</label> <input type="password" class="form-control" name="password" value="${password}" />
						</div>
						<div class="form-group">
							<label>Fullname:</label> <input type="text" class="form-control" name="fullname" value="${fullname}" />
						</div>
						<div class="form-group">
							<label>Email Address:</label> <input type="text" class="form-control" name="email" value="${email}" />
						</div>
						<div class="form-check form-check-inline">
							<label>Role:&nbsp;&nbsp;&nbsp;</label> 
							<label class="ms-4 me-4"><input type="radio" class="form-check-input"
								name="admin" value="true" ${admin ? 'checked' : ''} />Admin </label> 
								<label class="ms-2">
								<input
								type="radio" class="form-check-input" name="admin" value="false" ${!admin ? 'checked' : ''} />User </label>
						</div>
						<div class="form-group pt-2 pb-3">
							<button class="btn btn-primary btn-Create"  formaction="user/create">Create</button>
							<button class="btn btn-warning btn-Update" formaction="user/update">Update</button>
							<button class="btn btn-danger btn-Delete" formaction="user/delete" disabled="disabled">Delete</button>
							<button class="btn btn-info btn-Reset " formaction="user/reset">Reset</button>
						<button class="btn btn-info btn-Find" formaction="user/FindbyID">Find</button>
						</div>
					</form>
				</div>
			</div>
			<div class="row pt-0 pl-3 pr-3">
				<table class="table border ">
					<thead class="thead-dark font-weight-bold text-dark" style="background-color: #E5E5E5;">
						<tr class="">
							<td>User ID</td>
							<td>Password</td>
							<td>Fullname</td>
							<td>Email</td>
							<td>Role</td>
							<td>&nbsp;</td>
						</tr>
					</thead>
					<tbody>
						<c:forEach var="item" items="${users}">
							<tr class="thead-dark    text-dark">
								<td>${item.id}</td>
								<td>${item.password}</td>
								<td>${item.fullname}</td>
								<td>${item.email}</td>
								<td>${item.admin ? 'Admin' : 'User'}</td>
								<td><a href="user/edit?id=${item.id}" class="btn-Edit">Edit</a> | <a href="user/delete?id=${item.id}" >Delete</a></td>
							</tr>
						</c:forEach>
					<tbody>
				</table>
			</div>
		</div>
	</div>
	<!-- Optional JavaScript -->
	<!-- jQuery first, then Popper.js, then Bootstrap JS -->
	<script src="https://cdn.jsdelivr.net/npm/jquery@3.5.1/dist/jquery.slim.min.js"></script>
	<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js"></script>
	<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/js/bootstrap.bundle.min.js"></script>
</body>
</html>